jQuery(document).ready(function($){

var accordion = {
    init: function() {
    	// JSON Proppeller
        this.accordionJson = 'http://design.propcom.co.uk/buildtest/accordion-data.json';
        this.getData();
    },
    // AJAX Request
    getData: function() {
        $.ajax({
            url: accordion.accordionJson,
            success: function(data) {
                var dataBlock = data.blocks;

                accordion.wrapper(dataBlock);
                accordion.hideAccordion();
            }
        });
    },

    // Accordion Wrapper
    wrapper: function(accordionData) {
        for ( i = 0; i < accordionData.length; i++) {
            $( ".accordion" ).append( "<section>" );
        }

        accordion.appendContents(accordionData);
    },

    // Append JSON Data contents
    appendContents: function(accordionData) {
        $('section').each(function(index) {
            $('section').eq(index).append('<h2>'+accordionData[index]['heading']+'</h2>');
            $('section').eq(index).append('<div class="content">'+accordionData[index]['content']+'</div>');

        });
    },

    // Hide
    hideAccordion: function() {
        $('.content').hide();
        accordion.toggeling();
    },

    // Toggling Accordion
    toggeling: function() {
        $('section').on('click', function() {
            $(this).find('.content').slideToggle();
            $(this).toggleClass('active');
        });
    },
};
accordion.init();
});



